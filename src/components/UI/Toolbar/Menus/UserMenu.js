import React, {Fragment} from 'react';
import {MenuItem, Nav, NavDropdown} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const UserMenu = ({user, logout}) => {
    const navTitle = (
        <Fragment>
            Hello, <b>{user.username}</b>!
        </Fragment>
    );

    return (
        <Nav pullRight>
            <NavDropdown title={navTitle} id="user-menu">
                <LinkContainer to="/tracks-history" exact>
                    <MenuItem>Track history</MenuItem>
                </LinkContainer>
                <MenuItem divider/>
                <LinkContainer to="/add-artist" exact>
                    <MenuItem>Add artist</MenuItem>
                </LinkContainer>
                <MenuItem divider/>
                <LinkContainer to="/add-album" exact>
                    <MenuItem>Add album</MenuItem>
                </LinkContainer>
                <MenuItem divider/>
                <LinkContainer to="/add-track" exact>
                    <MenuItem>Add track</MenuItem>
                </LinkContainer>
                <MenuItem divider/>
                <MenuItem onClick={logout}>Logout</MenuItem>
            </NavDropdown>
        </Nav>
    )
};

export default UserMenu;
