import React from 'react';
import {Button, ButtonToolbar, Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

import notFound from '../../assets/images/not_found.png';

const ArtistListItem = props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + '/uploads/' + props.image;
    }

    return (
        <Panel>
            <Panel.Body>
                <Image
                    style={{width: '100px', marginRight: '10px'}}
                    src={image}
                    thumbnail
                />
                <Link to={'/albums/' + props.id}>
                    {props.title}
                </Link>

                {props.published ? null : <span style={{color: 'red'}}>Not published</span>}
                <ButtonToolbar>
                    {props.remove && <Button bsStyle="danger" onClick={props.remove}>Delete</Button>}
                    {!props.published && <Button bsStyle="primary" onClick={props.publish}>Publish</Button>}
                </ButtonToolbar>
            </Panel.Body>
        </Panel>
    );
};

ArtistListItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string
};

export default ArtistListItem;