import React, {Component, Fragment} from 'react';
import {Button, ButtonToolbar, PageHeader, Panel} from "react-bootstrap";
import {connect} from 'react-redux';
import {addTrack, deleteTrack, fetchTracks, publishTrack} from "../../store/actions/tracks";


class Tracks extends Component {

    componentDidMount() {
        this.props.onFetchTracks(this.props.match.params.id);
    }

    render() {
        let listTracks = (
            this.props.tracks.map(track => {
                if (track.published) {
                    return (
                        <Panel key={track._id}>
                            <Panel.Body>
                                <p onClick={this.props.user ?
                                    () => this.props.onTrackAdded({track: track._id}, this.props.user.token)
                                    : null}
                                   style={{cursor: 'pointer'}}>
                                    <b>{track.number}</b> {track.title}
                                </p>
                            </Panel.Body>
                        </Panel>
                    )
                } else {
                    return null;
                }
            })
        );

        if (this.props.user && this.props.user.role === 'admin') {
            listTracks = (
                this.props.tracks.map(track => (
                    <Panel key={track._id}>
                        <Panel.Body>
                            <p onClick={this.props.user ?
                                () => this.props.onTrackAdded({track: track._id}, this.props.user.token)
                                : null}
                               style={{cursor: 'pointer'}}>
                                <b>{track.number}</b> {track.title}
                            </p>
                            {track.published ? null : <span style={{color: 'red'}}>Not published</span>}
                            <ButtonToolbar>
                                <Button bsStyle="danger" onClick={() => this.props.deleteTrack(track._id)}>Delete</Button>
                                {!track.published &&
                                <Button bsStyle="primary"
                                        onClick={() => this.props.publishTrack(track._id)}
                                >Publish
                                </Button>}
                            </ButtonToolbar>
                        </Panel.Body>
                    </Panel>
                ))
            );
        }

        return (
            <Fragment>
                <PageHeader>Tracks</PageHeader>
                {listTracks}
            </Fragment>
        )
    }
}


const mapStateToProps = state => {
    return {
        tracks: state.tracks.tracks,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchTracks: id => dispatch(fetchTracks(id)),
        onTrackAdded: (trackData, token) => dispatch(addTrack(trackData, token)),
        deleteTrack: id => dispatch(deleteTrack(id)),
        publishTrack: id => dispatch(publishTrack(id))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Tracks);