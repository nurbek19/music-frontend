import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {addAlbum} from "../../store/actions/albums";
import {fetchArtists} from "../../store/actions/artists";

class AddAlbum extends Component {
    state = {
        name: '',
        artist: '',
        releaseYear: '',
        image: ''
    };

    componentDidMount() {
        this.props.fetchArtists();
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.addAlbum(formData);
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {

        return (
            <Fragment>
                <PageHeader>Add new album</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>

                    <FormElement
                        propertyName="name"
                        title="Title"
                        placeholder="Title"
                        autoComplete="new-title"
                        type="text"
                        value={this.state.name}
                        changeHandler={this.inputChangeHandler}
                        required
                    />

                    <FormElement
                        propertyName="artist"
                        title="Artist"
                        type="select"
                        options={this.props.artists}
                        value={this.state.artist}
                        changeHandler={this.inputChangeHandler}
                        required
                    />

                    <FormElement
                        propertyName="releaseYear"
                        title="Release year"
                        placeholder="Year"
                        autoComplete="new-year"
                        type="text"
                        value={this.state.releaseYear}
                        changeHandler={this.inputChangeHandler}
                    />

                    <FormElement
                        propertyName="image"
                        title="Image"
                        type="file"
                        changeHandler={this.fileChangeHandler}
                        required
                    />


                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Save</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
  return {
      artists: state.artists.artists
  }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchArtists: () => dispatch(fetchArtists()),
        addAlbum: albumData => dispatch(addAlbum(albumData))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddAlbum);