import React, {Component} from 'react';
import {Panel} from "react-bootstrap";
import {connect} from 'react-redux';
import {fetchTrackHistories} from "../../store/actions/trackHistories";

class TrackHistory extends Component {

    componentDidMount() {
        if (!this.props.user) {
            this.props.history.push('/login');
        } else {
            this.props.onFetchTrackHistories(this.props.user.token);
        }
    }

    render() {
        return (
            <div>
                {this.props.trackHistories.map(track => (
                    <Panel key={track._id}>
                        <Panel.Body>
                            <h4>{track.track.album.artist.name}</h4>
                            <p>{track.track.title}</p>
                            <p><b>{track.datetime}</b></p>
                        </Panel.Body>
                    </Panel>
                ))}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        trackHistories: state.trackHistories.trackHistories,
        user: state.users.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchTrackHistories: token => dispatch(fetchTrackHistories(token))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);