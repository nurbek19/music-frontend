import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import ProductListItem from '../../components/ArtistListItem/ArtistListItem';
import {deleteArtist, fetchArtists, publishArtist} from "../../store/actions/artists";
import {PageHeader} from "react-bootstrap";

class Artists extends Component {
    componentDidMount() {
        this.props.onFetchArtists();
    }

    publishArtist = (id) => {
        this.props.publishArtist(id).then(() => {
            this.props.onFetchArtists();
        })
    };

    deleteArtist = id => {
        this.props.deleteArtist(id).then(() => {
            this.props.onFetchArtists();
        })
    };

    render() {
        let listArtists = (
            this.props.artists.map(artist => {
                if (artist.published) {
                    return <ProductListItem
                        key={artist._id}
                        id={artist._id}
                        title={artist.name}
                        image={artist.image}
                        published={artist.published}
                    />
                } else {
                    return null;
                }

            })
        );

        if (this.props.user && this.props.user.role === 'admin') {
            listArtists = (
                this.props.artists.map(artist => (
                    <ProductListItem
                        key={artist._id}
                        id={artist._id}
                        title={artist.name}
                        image={artist.image}
                        published={artist.published}
                        remove={() => this.deleteArtist(artist._id)}
                        publish={() => this.publishArtist(artist._id)}
                    />
                )))
        }

        return (
            <Fragment>
                <PageHeader>Artists</PageHeader>
                {listArtists}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        artists: state.artists.artists,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchArtists: () => dispatch(fetchArtists()),
        deleteArtist: id => dispatch(deleteArtist(id)),
        publishArtist: id => dispatch(publishArtist(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Artists);