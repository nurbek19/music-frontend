import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {addArtist} from "../../store/actions/artists";

class AddArtist extends Component {
    state = {
        name: '',
        description: '',
        image: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.addArtist(formData);
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {

        return (
            <Fragment>
                <PageHeader>Add new artist</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>

                    <FormElement
                        propertyName="name"
                        title="Name"
                        placeholder="Artist name"
                        autoComplete="new-name"
                        type="text"
                        value={this.state.name}
                        changeHandler={this.inputChangeHandler}
                        required
                    />

                    <FormElement
                        propertyName="description"
                        title="Description"
                        placeholder="Description"
                        autoComplete="new-description"
                        type="textarea"
                        value={this.state.description}
                        changeHandler={this.inputChangeHandler}
                    />

                    <FormElement
                        propertyName="image"
                        title="Image"
                        type="file"
                        changeHandler={this.fileChangeHandler}
                        required
                    />


                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Save</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        addArtist: artistData => dispatch(addArtist(artistData))
    }
};

export default connect(null, mapDispatchToProps)(AddArtist);