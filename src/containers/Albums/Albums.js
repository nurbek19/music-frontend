import React, {Component, Fragment} from 'react';
import {Button, ButtonToolbar, Image, PageHeader, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import {connect} from 'react-redux';
import {deleteAlbum, fetchAlbums, publishAlbum} from "../../store/actions/albums";
import config from "../../config";


class Albums extends Component {

    componentDidMount() {
        this.props.onFetchAlbums(this.props.match.params.id);
    }

    render() {
        let listAlbums = (
            this.props.albums.map(album => {
                if (album.published) {
                    return (
                        <Panel key={album._id}>
                            <Panel.Body>
                                <p><Link to={'/tracks/' + album._id}>{album.name}</Link></p>
                                {album.image && <Image
                                    style={{width: '100px', marginRight: '10px'}}
                                    src={config.apiUrl + '/uploads/' + album.image}
                                    thumbnail
                                />}

                            </Panel.Body>
                        </Panel>)
                } else {
                    return null;
                }
            })
        );

        if (this.props.user && this.props.user.role === 'admin') {
            listAlbums = (
                this.props.albums.map(album => (
                    <Panel key={album._id}>
                        <Panel.Body>
                            <p><Link to={'/tracks/' + album._id}>{album.name}</Link></p>
                            {album.image && <Image
                                style={{width: '100px', marginRight: '10px'}}
                                src={config.apiUrl + '/uploads/' + album.image}
                                thumbnail
                            />}
                            {album.published ? null : <span style={{color: 'red'}}>Not published</span>}
                            <ButtonToolbar>
                                <Button bsStyle="danger"
                                        onClick={() => this.props.deleteAlbum(album._id)}>Delete</Button>
                                {!album.published &&
                                <Button bsStyle="primary"
                                        onClick={() => this.props.publishAlbum(album._id)}
                                >Publish
                                </Button>}
                            </ButtonToolbar>
                        </Panel.Body>
                    </Panel>
                )))
        }

        return (
            <Fragment>
                <PageHeader>Albums</PageHeader>
                {listAlbums}
            </Fragment>
        )
    }
}


const mapStateToProps = state => {
    return {
        albums: state.albums.albums,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchAlbums: id => dispatch(fetchAlbums(id)),
        deleteAlbum: id => dispatch(deleteAlbum(id)),
        publishAlbum: id => dispatch(publishAlbum(id))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Albums);