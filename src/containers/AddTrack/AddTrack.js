import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {fetchAllAlbums} from "../../store/actions/albums";
import {createTrack} from "../../store/actions/tracks";

class AddTrack extends Component {
    state = {
        title: '',
        album: '',
        duration: '',
        number: ''
    };

    componentDidMount() {
        this.props.fetchAllAlbums();
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.createTrack(this.state);
    };

    render() {

        return (
            <Fragment>
                <PageHeader>Add new album</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>

                    <FormElement
                        propertyName="title"
                        title="Title"
                        placeholder="Title"
                        autoComplete="new-title"
                        type="text"
                        value={this.state.title}
                        changeHandler={this.inputChangeHandler}
                        required
                    />

                    <FormElement
                        propertyName="album"
                        title="Album"
                        type="select"
                        options={this.props.allAlbums}
                        value={this.state.album}
                        changeHandler={this.inputChangeHandler}
                        required
                    />

                    <FormElement
                        propertyName="duration"
                        title="Duration"
                        placeholder="Duration"
                        autoComplete="new-duration"
                        type="text"
                        value={this.state.duration}
                        changeHandler={this.inputChangeHandler}
                    />

                    <FormElement
                        propertyName="number"
                        title="Number"
                        placeholder="Number"
                        autoComplete="new-number"
                        type="text"
                        value={this.state.number}
                        changeHandler={this.inputChangeHandler}
                    />


                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Save</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        allAlbums: state.albums.allAlbums
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchAllAlbums: () => dispatch(fetchAllAlbums()),
        createTrack: trackData => dispatch(createTrack(trackData))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTrack);