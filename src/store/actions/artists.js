import axios from '../../axios-api';

import {
    ADD_ARTIST_SUCCESS, DELETE_ARTIST_SUCCESS,
    FETCH_ARTISTS_SUCCESS, PUBLISH_ARTIST_SUCCESS
} from "./actionTypes";
import {push} from "react-router-redux";


export const fetchProductsSuccess = artists => {
    return {type: FETCH_ARTISTS_SUCCESS, artists};
};

export const fetchArtists = () => {
    return dispatch => {
        axios.get('/artists').then(
            response => dispatch(fetchProductsSuccess(response.data))
        );
    }
};

export const addArtistSuccess = () => {
    return {type: ADD_ARTIST_SUCCESS}
};

export const addArtist = artistData => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.post('/artists', artistData, {headers}).then(() => {
            dispatch(addArtistSuccess());
            dispatch(push('/'));
        })
    }
};


export const deleteArtistSuccess = () => {
    return {type: DELETE_ARTIST_SUCCESS}
};


export const deleteArtist = id => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.delete(`/artists/${id}`, {headers}).then(() => {
            dispatch(deleteArtistSuccess());
            dispatch(push('/'));
        })
    }
};

export const publishArtistSuccess = () => {
    return {type: PUBLISH_ARTIST_SUCCESS}
};

export const publishArtist = id => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.put(`/artists/${id}`, null, {headers}).then(() => {
            dispatch(publishArtistSuccess());
        })
    }
};