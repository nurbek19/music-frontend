import axios from '../../axios-api';
import {
    ADD_ALBUM_SUCCESS, DELETE_ALBUM_SUCCESS,
    FETCH_ALBUMS_SUCCESS,
    FETCH_ALL_ALBUMS_SUCCESS, PUBLISH_ALBUM_SUCCESS,
} from "./actionTypes";
import {push} from "react-router-redux";

export const fetchAlbumsSuccess = albums => {
  return {type: FETCH_ALBUMS_SUCCESS, albums}
};

export const fetchAlbums = id => {
    return dispatch => {
        axios.get(`/albums?artist=${id}`).then(
            response => dispatch(fetchAlbumsSuccess(response.data))
        );
    }
};

export const fetchAllAlbumsSuccess = allAlbums => {
  return {type: FETCH_ALL_ALBUMS_SUCCESS, allAlbums}
};

export const fetchAllAlbums = () => {
    return dispatch => {
        axios.get('/albums').then(
            response => dispatch(fetchAllAlbumsSuccess(response.data))
        );
    }
};

export const addAlbumSuccess = () => {
    return {type: ADD_ALBUM_SUCCESS}
};

export const addAlbum = albumData => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.post('/albums', albumData, {headers}).then(() => {
            dispatch(addAlbumSuccess());
            dispatch(push('/'));
        })
    }
};


export const deleteAlbumSuccess = () => {
    return {type: DELETE_ALBUM_SUCCESS}
};


export const deleteAlbum = id => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.delete(`/albums/${id}`, {headers}).then(() => {
            dispatch(deleteAlbumSuccess());
            dispatch(push('/'));
        })
    }
};

export const publishAlbumSuccess = () => {
    return {type: PUBLISH_ALBUM_SUCCESS}
};

export const publishAlbum = id => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.put(`/albums/${id}`, null, {headers}).then(() => {
            dispatch(publishAlbumSuccess());
            dispatch(push('/'));
        })
    }
};















