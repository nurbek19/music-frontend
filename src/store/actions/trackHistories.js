import axios from '../../axios-api';
import {FETCH_TRACK_HISTORIES_SUCCESS} from "./actionTypes";

export const fetchTrackHistoriesSuccess = trackHistories => {
    return {type: FETCH_TRACK_HISTORIES_SUCCESS, trackHistories};
}

export const fetchTrackHistories = token => {
    return dispatch => {
        const headers = {'Token': token};

        axios.get('/track_history', {headers}).then(response => {

            dispatch(fetchTrackHistoriesSuccess(response.data))
        })
    }
}