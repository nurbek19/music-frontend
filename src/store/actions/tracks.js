import axios from '../../axios-api';
import {
    ADD_TRACK_SUCCESS,
    CREATE_TRACK_SUCCESS,
    DELETE_TRACK_SUCCESS,
    FETCH_TRACKS_SUCCESS,
    PUBLISH_TRACK_SUCCESS
} from "./actionTypes";
import {push} from "react-router-redux";

export const fetchTracksSuccess = tracks => {
    return {type: FETCH_TRACKS_SUCCESS, tracks}
};

export const fetchTracks = id => {
    return dispatch => {
        axios.get(`/tracks?album=${id}`).then(
            response => {
                dispatch(fetchTracksSuccess(response.data))
            }
        );
    }
};

export const addTrackSuccess = () => {
    return {type: ADD_TRACK_SUCCESS};
};

export const addTrack = (trackData, token) => {
    return (dispatch, getState) => {
        const headers = {'Token': token};

        axios.post('/track_history', trackData, {headers}).then(response => {
            dispatch(addTrackSuccess());
        })
    }
};

export const CreateTrackSuccess = () => {
    return {type: CREATE_TRACK_SUCCESS}
};

export const createTrack = trackData => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.post('/tracks', trackData, {headers}).then(() => {
            dispatch(CreateTrackSuccess());
            dispatch(push('/'));
        })
    }
};

export const deleteTrackSuccess = () => {
    return {type: DELETE_TRACK_SUCCESS}
};


export const deleteTrack = id => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.delete(`/tracks/${id}`, {headers}).then(() => {
            dispatch(deleteTrackSuccess());
            dispatch(push('/'));
        })
    }
};

export const publishTrackSuccess = () => {
    return {type: PUBLISH_TRACK_SUCCESS}
};

export const publishTrack = id => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};

        return axios.put(`/tracks/${id}`, null, {headers}).then(() => {
            dispatch(publishTrackSuccess());
            dispatch(push('/'));
        })
    }
};

















