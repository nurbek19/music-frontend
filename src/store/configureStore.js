import usersReducer from "./reducers/users";
import {routerMiddleware, routerReducer} from "react-router-redux";
import thunkMiddleware from "redux-thunk";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import albumsReducer from "./reducers/albums";
import createHistory from "history/createBrowserHistory";
import tracksReducer from "./reducers/tracks";
import trackHistoriesReducer from "./reducers/trackHistories";
import artistsReducer from "./reducers/artists";
import {loadState, saveState} from "./reducers/localStorage";

const rootReducer = combineReducers({
    artists: artistsReducer,
    users: usersReducer,
    albums: albumsReducer,
    tracks: tracksReducer,
    trackHistories: trackHistoriesReducer,
    routing: routerReducer
});

export const history = createHistory();

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveState({
        users: {
            user: store.getState().users.user
        }
    });
});

export default store;