import {FETCH_ALBUMS_SUCCESS, FETCH_ALL_ALBUMS_SUCCESS} from "../actions/actionTypes";

const initialState = {
    albums: [],
    allAlbums: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_ALBUMS_SUCCESS:
            return {...state, albums: action.albums};
        case FETCH_ALL_ALBUMS_SUCCESS:
            return {...state, allAlbums: action.allAlbums};
        default:
            return state;
    }
};

export default reducer;