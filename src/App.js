import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Layout from "./containers/Layout/Layout";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import AddArtist from "./containers/AddArtist/AddArtist";
import AddAlbum from "./containers/AddAlbum/AddAlbum";
import AddTrack from "./containers/AddTrack/AddTrack";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Artists}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/albums/:id" exact component={Albums}/>
                    <Route path="/tracks/:id" exact component={Tracks}/>
                    <Route path="/tracks-history" exact component={TrackHistory}/>
                    <Route path="/add-artist" exact component={AddArtist}/>
                    <Route path="/add-album" exact component={AddAlbum}/>
                    <Route path="/add-track" exact component={AddTrack}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
